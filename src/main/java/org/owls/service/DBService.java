package org.owls.service;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import javax.xml.crypto.Data;

/**
 * Created by juneyoungoh on 2018. 2. 18..
 */
@Service
public class DBService {
    private DataSource mySqlDataSource() {
        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName("com.mysql.jdbc.Driver");
        basicDataSource.setUsername("secret");
        basicDataSource.setPassword("secret");
        basicDataSource.setUrl("jdbc:mysql://secret.com:3306/secret?zeroDateTimeBehavior=convertToNull");
        basicDataSource.setConnectionProperties("useSSL=false");

        basicDataSource.setInitialSize(50);
        basicDataSource.setMaxTotal(100);
        basicDataSource.setDefaultAutoCommit(true);
        basicDataSource.setRemoveAbandonedOnBorrow(true);
        basicDataSource.setMaxWaitMillis(3000);
        basicDataSource.setTestWhileIdle(true);
        basicDataSource.setTestOnBorrow(false);
        basicDataSource.setTestOnReturn(false);
        basicDataSource.setNumTestsPerEvictionRun(3);
        basicDataSource.setMinEvictableIdleTimeMillis(-1);
        basicDataSource.setValidationQueryTimeout(7);
        basicDataSource.setTestWhileIdle(true);
        basicDataSource.setTimeBetweenEvictionRunsMillis(60000);
        return basicDataSource;
    }

    public DataSource getDS(String dsId) {
        return mySqlDataSource();
    }
}
