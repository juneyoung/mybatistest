package org.owls.service;

import org.apache.ibatis.binding.MapperRegistry;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.Logger;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

/**
 * Created by juneyoungoh on 2018. 2. 18..
 *
 * DS 인스턴스를 넘기면 ApplicationContext 를 뒤져서
 * 0. 해당 SqlSessionTemplate 가 있으면 반환하고
 * 1. 없으면 생성해서 반환함
 *
 * 특징
 * 0. 이 클래스는 싱글톤으로 관리되며
 * 1. SqlSessionFactory 를 관리하고
 * 1. SqlSessionTemplate 를 제공한다
 *
 * http://www.mybatis.org/mybatis-3/getting-started.html
 */

@Service
public class MybatisService implements ApplicationContextAware {


    private Logger logger = Logger.getLogger(MybatisService.class);
    private ApplicationContext context;
    private String[] mapperScanPackages = {};


    @Autowired
    DBService dbService;

    @Autowired
    Environment env;


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }

    /*
    * Checking ice-mybatis environment
    * */
    @PostConstruct
    public void init (){
        try {
            mapperScanPackages = env.getProperty("ice-mybatis.mapper-scan", String[].class);
        } catch (Exception e) {
            logger.info("Failed to load ice-mybatis configuration in yml");
            logger.error(e);
        }
    }


    // Register a Object with a name, if not exists
    private void registerBean(String beanName, Object newBean){
        BeanDefinitionRegistry registry = (BeanDefinitionRegistry)  context.getAutowireCapableBeanFactory();
        BeanDefinitionBuilder builder = BeanDefinitionBuilder.rootBeanDefinition(newBean.getClass());
        BeanDefinition def = builder.getBeanDefinition();
        logger.info("Register bean named with :: " + beanName);
        if(!registry.containsBeanDefinition(beanName)) registry.registerBeanDefinition(beanName, def);
    }


    /*
    * Managing SqlSessionFactory
    * */
    private SqlSessionFactory generateSqlSessionFactory(String dsId, DataSource ds) throws Exception {
        String beanName = dsId + "SqlSessionFactory";

        SqlSessionFactory sqlSessionFactory = null;
        try {
            Object registeredBean = context.getBean(beanName);
            logger.info("Found SqlSessionFactory bean [ " + beanName + " ]. Returns the instance");
            sqlSessionFactory = (SqlSessionFactory) registeredBean;
        } catch (NoSuchBeanDefinitionException e) {
            logger.info("Not found SqlSessionFactory bean [ " + beanName + " ]. Generate new one");
            SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
            sqlSessionFactoryBean.setDataSource(ds);
            sqlSessionFactory = (SqlSessionFactory) sqlSessionFactoryBean.getObject();
            registerBean(beanName, sqlSessionFactory);
        }

        // FOR TEST 0219
        MapperRegistry registry = sqlSessionFactory.getConfiguration().getMapperRegistry();

        for(String aPackage : mapperScanPackages) {
            logger.info("Register Mapper Package :: [ " + aPackage + " ] to Bean :: [ " + beanName + " ]");
            sqlSessionFactory.getConfiguration().getMapperRegistry().addMappers(aPackage);
        }

        logger.info("=== Printing Mapper Registry starts");
        registry.getMappers().forEach(mapper -> {
            logger.info("Mapper Registry :: " +  mapper.getName());
        });
        logger.info("=== Printing Mapper Registry ends");
        // FOR TEST 0219
        return sqlSessionFactory;
    }


    /*
    * Managing SqlSessionTemplate
    * */
    private SqlSessionTemplate generateSqlSessionTemplate(String dsId, SqlSessionFactory sqlSessionFactory) throws Exception {
        String beanName = dsId + "SqlSessionTemplate";
        SqlSessionTemplate sqlSessionTemplate = null;
        try {
            Object registeredBean = context.getBean(beanName);
            logger.info("Found SqlSessionTemplate bean [ " + beanName + " ]. Returns the instance");
            sqlSessionTemplate = (SqlSessionTemplate) registeredBean;
        } catch (NoSuchBeanDefinitionException e) {
            logger.info("Not found SqlSessionTemplate bean [ " + beanName + " ]. Generate new one");
            sqlSessionTemplate = new SqlSessionTemplate(sqlSessionFactory);
            registerBean(beanName, sqlSessionFactory);
        }
        return sqlSessionTemplate;
    }


    public SqlSessionTemplate getSqlSessionTemplate(String dsId) {
        SqlSessionTemplate template = null;
        try {
            DataSource ds = dbService.getDS(dsId);
            SqlSessionFactory sqlSessionFactory = generateSqlSessionFactory(dsId, ds);
            template = generateSqlSessionTemplate(dsId, sqlSessionFactory);
        } catch (Exception e) {
            logger.error("Failed to fetch SqlSessionFactory. returns null value ", e);
        }
        return template;
    }
}
