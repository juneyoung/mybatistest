package org.owls.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.mybatis.spring.SqlSessionTemplate;
import org.owls.service.MybatisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.Map;

/**
 * Created by juneyoungoh on 2018. 2. 15..
 */
@Controller
@RequestMapping(value = {"/test"})
public class TestController {

    @Autowired
    MybatisService mybatisService;
    private SqlSessionTemplate sqlSessionTemplate;

    private Logger logger = Logger.getLogger(TestController.class);

    @PostConstruct
    public void init(){
        sqlSessionTemplate = mybatisService.getSqlSessionTemplate("test");
    }

    @RequestMapping(value = {"getCurrentTimestamp"})
    public @ResponseBody String getCurrentTimestamp() throws Exception {

        logger.info("=== SqlSessionTemplate Info in Controller :: " + sqlSessionTemplate);

//        sqlSessionTemplate.select("SELECT NOW() FROM DUAL", null);
        Map<String, Object> data = (Map) sqlSessionTemplate.selectOne("org.owls.mybatis.mapper.TestMapper.selectTest", new Date());
        logger.info("dataMap :: " + data);

        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(data);
    }
};