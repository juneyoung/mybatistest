package org.owls.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.mybatis.spring.SqlSessionTemplate;
import org.owls.service.MybatisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;
import java.util.Map;

/**
 * Created by juneyoungoh on 2018. 2. 19..
 */
@Controller
@RequestMapping(value = {"test2"})
public class TestController2 {

    @Autowired
    MybatisService mybatisService;
    private SqlSessionTemplate sqlSessionTemplate;

    private Logger logger = Logger.getLogger(TestController.class);

    @PostConstruct
    public void init(){
        sqlSessionTemplate = mybatisService.getSqlSessionTemplate("test2");
    }

    @RequestMapping(value = {"getCurrentTimestamp"})
    public @ResponseBody
    String getCurrentTimestamp() throws Exception {

        logger.info("=== SqlSessionTemplate Info in Controller2 :: " + sqlSessionTemplate);
        Map<String, Object> data = (Map) sqlSessionTemplate.selectOne("org.owls.mybatis.mapper.TestMapper.selectTimestamp");
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(data);
    }
}
