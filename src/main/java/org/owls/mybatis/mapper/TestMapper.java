package org.owls.mybatis.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.Map;

/**
 * Created by juneyoungoh on 2018. 2. 18..
 */
@Mapper
public interface TestMapper {
    @Select("SELECT NOW() FROM DUAL")
    public Map selectTimestamp() throws Exception;


    @Select("SELECT NOW() > #{date} FROM DUAL")
    public Map selectTest(@Param("date")Date date) throws Exception;
}
