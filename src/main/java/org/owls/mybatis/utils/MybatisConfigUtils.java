package org.owls.mybatis.utils;

import org.apache.ibatis.binding.MapperRegistry;
import org.apache.ibatis.session.Configuration;
import org.mybatis.spring.SqlSessionTemplate;

/**
 * Created by juneyoungoh on 2018. 2. 19..
 * Configuration 수정
 */
public class MybatisConfigUtils {

    public static Configuration getConfiguration(SqlSessionTemplate sessionTemplate) {
        return sessionTemplate.getConfiguration();
    }

    public static MapperRegistry getMapperRegistry(SqlSessionTemplate sessionTemplate){
        return getConfiguration(sessionTemplate).getMapperRegistry();
    }

    /*
    * Register a Mapper Object to current SqlSession
    * Caution : This is a permanent action. I does not remove new mapper automatically after task
    * */
    public static void registerDynamicMapper(SqlSessionTemplate sessionTemplate, Object newMapper) {
        getMapperRegistry(sessionTemplate).addMapper(newMapper.getClass());
    }

    /*
    * Register a Mapper Packages
    * */
    public static void registerDynamicMapperPackage(SqlSessionTemplate sessionTemplate, String packageName) {
        getMapperRegistry(sessionTemplate).addMappers(packageName);
    }
}
