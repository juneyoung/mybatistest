package org.owls.mybatis.model;

/**
 * Created by juneyoungoh on 2018. 2. 18..
 */
public class City {

    public Integer id;
    public String state;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String name) {
        this.state = name;
    }
}
